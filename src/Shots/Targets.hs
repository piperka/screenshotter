{-# LANGUAGE OverloadedStrings #-}

module Shots.Targets where

import Contravariant.Extras
import Data.Int
import Data.Text (Text)
import Data.Vector (Vector)
import Hasql.Connection
import qualified Hasql.Decoders as D
import qualified Hasql.Encoders as E
import Hasql.Session hiding (sql)
import Hasql.Statement

loadTargets
  :: Int32
  -> Session (Vector (Int32, Text))
loadTargets = flip statement $ Statement sql (E.param E.int4)
  (D.rowVector $ (,) <$> D.column D.int4 <*> D.column D.text) True
  where
    sql = "SELECT ord, url_base||name||url_tail \
          \FROM updates JOIN comics USING (cid) \
          \WHERE name IS NOT NULL AND cid=$1"

loadTargetsOnlyMissing
  :: Int32
  -> Session (Vector (Int32, Text))
loadTargetsOnlyMissing = flip statement $ Statement sql (E.param E.int4)
  (D.rowVector $ (,) <$> D.column D.int4 <*> D.column D.text) True
  where
    sql = "SELECT ord, url_base||name||url_tail \
          \FROM updates JOIN comics USING (cid) \
          \WHERE name IS NOT NULL AND cid=$1 AND ord NOT IN \
          \(SELECT ord FROM screenshot_raw WHERE NOT deleted AND cid=$1)"

storeScreenshot
  :: Int32
  -> Int32
  -> Text
  -> Text
  -> Text
  -> Session ()
storeScreenshot cid ord url file title =
  statement (cid, ord, url, file, title) $
  Statement sql
  (contrazip5 (E.param E.int4) (E.param E.int4)
   (E.param E.text) (E.param E.text) (E.param E.text))
  D.unit True
  where
    sql = "INSERT INTO screenshot_raw (cid, ord, url, file, title) \
          \VALUES ($1, $2, $3, $4, $5)"

loadRaws
  :: Int32
  -> Session (Vector (Int32, Text))
loadRaws = flip statement $ Statement sql (E.param E.int4)
  (D.rowVector $ (,) <$> D.column D.int4 <*> D.column D.text) True
  where
    sql = "SELECT ord, file FROM (SELECT ord, file, deleted, \
          \ rank() OVER (PARTITION BY ord ORDER BY stamp DESC) AS r \
          \ FROM screenshot_raw WHERE cid=$1) AS x WHERE x.r=1 AND NOT deleted"

markThumbs
  :: Int32
  -> Session ()
markThumbs = flip statement $ Statement sql (E.param E.int4) D.unit True
  where
    sql = "WITH target AS ( \
          \SELECT ord, scr_id FROM (SELECT scr_id, ord, deleted, \
          \ rank() OVER (PARTITION BY ord ORDER BY stamp DESC) AS r \
          \ FROM screenshot_raw WHERE cid=$1) AS x WHERE x.r=1 AND NOT deleted \
          \), update_old AS ( \
          \UPDATE screenshot_raw SET thumb=false \
          \FROM target WHERE thumb=true AND cid=$1 AND screenshot_raw.ord=target.ord \
          \) \
          \UPDATE screenshot_raw SET thumb=true \
          \FROM target WHERE screenshot_raw.scr_id=target.scr_id"
