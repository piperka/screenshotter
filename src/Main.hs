{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE ScopedTypeVariables #-}

module Main where

import Control.Concurrent (threadDelay)
import Control.Concurrent.Async
import Control.Monad.Trans
import Control.Exception.Lifted
import qualified Data.Configurator as C
import Data.Configurator.Types (Config)
import Data.Int
import Data.Semigroup
import qualified Data.Text as T
import Data.Time.Clock
import Data.Time.Format
import qualified Data.Vector as V
import qualified Graphics.Image as I
import qualified Graphics.Image.IO as I
import Hasql.Connection
import Hasql.Session
import Network.HTTP.Client
import Options.Applicative
import System.Directory
import System.IO.Error (isAlreadyExistsError)
import System.Random
import Test.WebDriver

import Shots.Targets

import Debug.Trace

data ScreenshotterOptions = DownloadRaws
  { downloadCid :: Int32
  , onlyMissing :: Bool
  } | ProcessRaws
  { processCid :: Int32
  } deriving (Show)

screenshotterOptionsParser :: Parser ScreenshotterOptions
screenshotterOptionsParser = subparser $
  (command "download" $ flip info (progDesc "Download snapshots") $
   DownloadRaws
   <$> argument auto (metavar "CID")
   <*> switch (long "missing"
               <> short 'm')
  )
  <>
  (command "process" $ flip info (progDesc "Generate thumbnails") $
   ProcessRaws
   <$> argument auto (metavar "CID")
  )

main :: IO ()
main = do
  cfg <- C.load ["/srv/piperka.net/screenshotter.cfg"]
  screenshotter cfg =<< execParser opts
  where
    opts = info (screenshotterOptionsParser <**> helper)
           (fullDesc
            <> progDesc "Piperka Screenshotter"
           )

screenshotter
  :: Config
  -> ScreenshotterOptions
  -> IO ()
screenshotter cfg (ProcessRaws cid) = do
  settings <- C.lookupDefault "postgresql://kaol@/piperka" cfg "db"
  tgtPath <- (<> "/" <> show cid <> "/") <$>
    C.lookupDefault "/srv/piperka.net/files/thumbnails" cfg "thumbs"
  thumbWidth <- C.lookupDefault 200 cfg "thumbWidth"
  thumbRatio <- C.lookupDefault (2.0 :: Double) cfg "thumbRatio"
  catch (createDirectory tgtPath) $ \e -> do
    if isAlreadyExistsError e then return () else throw e
  bracket (either (error . show) return =<< acquire settings) release $ \conn -> do
    run (loadRaws cid) conn >>= either (error . show) return >>=
      (mapConcurrently_ $ \(ord, file) -> do
          img <- I.readImageRGB I.VS $ T.unpack file
          let (height, width) = I.dims img
              ratio = (fromIntegral height / fromIntegral width) :: Double
          I.writeImageExact I.JPG [I.JPGQuality 80]
            (tgtPath <> show ord <> ".jpg") $
            (I.resize I.Bilinear I.Edge
             (floor $ (fromIntegral thumbWidth) * min thumbRatio ratio, thumbWidth)) $
            (if ratio > thumbRatio
             then I.crop (0,0) (floor $ thumbRatio*fromIntegral width, width) else id) img)
    either (error . show) return =<< run (markThumbs cid) conn

screenshotter cfg (DownloadRaws cid missing) = do
  day <- formatTime defaultTimeLocale "%Y%m%d" <$> getCurrentTime
  let seleniumConfig = useBrowser chrome defaultConfig
      loader = (if missing then loadTargetsOnlyMissing else loadTargets) cid
  settings <- C.lookupDefault "postgresql://kaol@/piperka" cfg "db"
  tgtPath <- (<> "/" <> day <> "/") <$>
    C.lookupDefault "/srv/piperka.net/shots" cfg "raws"
  catch (createDirectory tgtPath) $ \e -> do
    if isAlreadyExistsError e then return () else throw e
  bracket (either (error . show) return =<< acquire settings) release $ \conn -> do
    let tgtPath' = tgtPath <> (show cid) <> "/"
    catch (createDirectory tgtPath') $ \e -> do
      if isAlreadyExistsError e then return () else throw e
    tgts <- either (error . show) return =<< (liftIO $ run loader conn)
    runSession seleniumConfig $ do
      V.mapM_ (\tgt -> do
                  let file = tgtPath' <> (show $ fst tgt)
                  catch (
                    do
                      openPage $ T.unpack $ snd tgt
                      title <- getTitle
                      saveScreenshot file
                      liftIO $
                        run (storeScreenshot cid (fst tgt) (snd tgt) (T.pack file) title) conn
                      return ()
                    ) (\e -> case e of
                          (x :: HttpException) -> liftIO $ print (cid, fst tgt, x)
                          _ -> throw e
                      )
                  -- Rest for a while
                  liftIO $ threadDelay =<< getStdRandom (randomR (1000000, 2000000))
              ) tgts
